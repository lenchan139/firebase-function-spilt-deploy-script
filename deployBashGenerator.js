"use strict";
/*

ISC License

Copyright (c) 2022 Len Chan(Evol Studio)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

GitLab Repo: https://gitlab.com/lenchan139/firebase-function-spilt-deploy-script
*/
exports.__esModule = true;
var fs = require("fs");
var path = require("path");
// config start
var indexJSPath = process.env.deployFirebaseIndexJSPath ? path.resolve(__dirname, process.env.deployFirebaseIndexJSPath) : "".concat(__dirname, "/src/index.ts"); // if not default, change it.
var handleFunctionEachTime = process.env.deployFirebaseHandleFunctionEachTime ? parseInt(process.env.deployFirebaseHandleFunctionEachTime) : 45; // if still failed decrease this number
var projectNickname = process.env.deployFirebaseProjectNickname ? process.env.deployFirebaseProjectNickname.split(",") : [
    "production",
    "default",
]; // if you want to deploy to multiple
// config end
// start the function
fs.readFile(indexJSPath, function (err, data) {
    if (err) {
        console.error(err);
    }
    else {
        var arrData = data.toString().split('\n');
        var arrFunctionNames = [];
        if ((arrData === null || arrData === void 0 ? void 0 : arrData.length) > 0) {
            var count_1 = 0;
            for (var _i = 0, arrData_1 = arrData; _i < arrData_1.length; _i++) {
                var line = arrData_1[_i];
                if (line &&
                    (line.indexOf("//") > line.indexOf('export')
                        ||
                            !line.includes('//'))
                    && /export[ ]+const[ ]+\w+[ ]+=/.test(line)) {
                    var arrFirst = line.split(' =');
                    if ((arrFirst === null || arrFirst === void 0 ? void 0 : arrFirst.length) > 0) {
                        var strFirst = arrFirst[0];
                        var arrZeroTwo = strFirst.split(' ');
                        if (arrZeroTwo.length > 1) {
                            var functionName = arrZeroTwo[arrZeroTwo.length - 1];
                            console.log("matched function name: ".concat(functionName));
                            arrFunctionNames.push(functionName);
                            count_1++;
                        }
                    }
                }
            }
            var arrContentNames = [];
            while (arrFunctionNames.length > 0) {
                arrContentNames.push(arrFunctionNames.splice(0, handleFunctionEachTime));
            }
            var contentOfFile = arrContentNames.map(function (v, i) {
                var strFunctionList = v.map(function (value, index) {
                    return "functions:".concat(value);
                }).join(',');
                return projectNickname.map(function (nickname) {
                    return "firebase deploy --only=".concat(strFunctionList, " --project=").concat(nickname.trim());
                }).join('&&');
            }).join("\n");
            fs.writeFile(__dirname + '/generatedDeploymentFile.sh', contentOfFile, function (err) {
                if (err) {
                    console.error('got error when saving depoly file', err);
                }
                else {
                    console.log("deployment file ok. \nCount of functions: ".concat(count_1));
                }
            });
        }
    }
});
