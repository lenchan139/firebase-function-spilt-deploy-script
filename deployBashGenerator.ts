/*

ISC License

Copyright (c) 2022 Len Chan(Evol Studio)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

GitLab Repo: https://gitlab.com/lenchan139/firebase-function-spilt-deploy-script
*/

import * as fs from 'fs'
import * as path from 'path'

// config start
const indexJSPath = process.env.deployFirebaseIndexJSPath ? path.resolve(__dirname, process.env.deployFirebaseIndexJSPath) : `${__dirname}/src/index.ts` // if not default, change it.
const handleFunctionEachTime = process.env.deployFirebaseHandleFunctionEachTime ? parseInt(process.env.deployFirebaseHandleFunctionEachTime) : 45 // if still failed decrease this number
const projectNickname = process.env.deployFirebaseProjectNickname ? process.env.deployFirebaseProjectNickname.split(",") : [
    "production",
    "default",
] // if you want to deploy to multiple


// config end

// start the function
fs.readFile(indexJSPath, (err: NodeJS.ErrnoException | null, data: Buffer) => {
    if (err) {
        console.error(err)
    } else {
        const arrData = data.toString().split('\n')
        const arrFunctionNames = []
        if (arrData?.length > 0) {
            let count = 0
            for (const line of arrData) {
                if (line &&
                    (line.indexOf("//") > line.indexOf('export')
                        ||
                        !line.includes('//')
                    )
                    && /export[ ]+const[ ]+\w+[ ]+=/.test(line)) {
                    const arrFirst = line.split(' =')
                    if (arrFirst?.length > 0) {
                        const strFirst = arrFirst[0]
                        const arrZeroTwo = strFirst.split(' ')
                        if (arrZeroTwo.length > 1) {
                            const functionName = arrZeroTwo[arrZeroTwo.length - 1]
                            console.log(`matched function name: ${functionName}`)
                            arrFunctionNames.push(functionName)
                            count++
                        }
                    }
                }
            }
            const arrContentNames: string[][] = []
            while (arrFunctionNames.length > 0) {
                arrContentNames.push(arrFunctionNames.splice(0, handleFunctionEachTime))
            }
            const contentOfFile = arrContentNames.map((v, i) => {
                const strFunctionList = v.map((value, index) => {
                    return `functions:${value}`
                }).join(',')
                return projectNickname.map(nickname => {
                    return `firebase deploy --only=${strFunctionList} --project=${nickname.trim()}`
                }).join('&&')
            }).join("\n")
            fs.writeFile(__dirname + '/generatedDeploymentFile.sh', contentOfFile, (err) => {
                if (err) {
                    console.error('got error when saving depoly file', err)
                } else {
                    console.log(`deployment file ok. \nCount of functions: ${count}`)
                }
            })
        }
    }
})