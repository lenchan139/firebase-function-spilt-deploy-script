# Why this?
Since I am facing a problem is when I running some of my firebase function project I got limit exceed when I deploying it. It is bacause firebase function have its limit rate about 50 function deployment each time. So it you  have many function in your firebase function project, it is easy to trigger "max limit rate" error.

# ok, how to use it?
First, you should install typescript compiler (tsc) for compile (or you can use js directly)
```bash
npm i -g typescript
```

You should copy deployBashGenerator.ts to your firebase functions folder inside your firebase proejct (not root, in your <project>/functions if init project with default config). and use tsc complie it to js file and run.
```bash 
tsc deployBashGenerator.ts
node deployBashGenerator.ts
```

It works with default settings as well.

# But I want to config something
Yes. There have some option you can config.
| Function Name | Description |
| ---- | ---- |
| deployFirebaseIndexJSPath | your firebase function index.js file path. it relative directory to deployBashGenerator.js file(use __dirname get then resolve). |
| deployFirebaseHandleFunctionEachTime | how many function deployed each time(only change it to smaller number when you still get limit exceed error) | 
| deployFirebaseProjectNickname | nickname of proejct that should be deployed. spilt by "," |

For example, if your index.js is and related to that deployBashGenerator.js file as ../functions/index.js. and, you want to deploy less function each time to 10 per time. and, you want to deploy to firebase project and its nickname are production, development and testsite.
you can chain it look like(remove option that you did not want to change):
``` bash
deployFirebaseIndexJSPath="../functions/index.js" deployFirebaseHandleFunctionEachTime="10" deployFirebaseProjectNickname="production,development,testsite" node deployBashGenerator.js
```

# Why not python? NodeJS sucks
Python sucker... Anyway, it is becasue firebase function should be coding in TypeScript and deploy as NodeJS node. I think NodeJS is must on your firebase function deploy environment. But, python is not the must in your deployment envrionment.

# License
Under ISC License.

```
ISC License

Copyright (c) 2022 Len Chan(Evol Studio)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

GitLab Repo: https://gitlab.com/lenchan139/firebase-function-spilt-deploy-script
 ```